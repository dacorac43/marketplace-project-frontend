import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

// Interceptors
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SaleComponent } from "./pages/sale/sale.component";
import { ProviderComponent } from './pages/provider/provider-detail/provider.component';
import { CartComponent } from './pages/cart/cart.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NewsComponent } from './pages/contents/news/news.component';
import { DiscountsComponent } from './pages/contents/discounts/discounts.component';
import { PostsComponent } from './pages/contents/posts/posts.component';
import { ListAdminsComponent } from './pages/admin/list-admins/list-admins.component';
import { AdminComponent } from './pages/admin/admin-detail/admin.component';
import { ListClientsComponent } from './pages/client/list-clients/list-clients.component';
import { ClientComponent } from './pages/client/client-detail/client.component';
import { ListProvidersComponent } from './pages/provider/list-providers/list-providers.component';
import { ProfileComponent } from './pages/profile/profile.component';



@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    NewsComponent,
    DiscountsComponent,
    PostsComponent,
    SaleComponent,
    ProviderComponent,
    CartComponent,
    ListAdminsComponent,
    AdminComponent,
    ListClientsComponent,
    ClientComponent,
    ListProvidersComponent,
    ProfileComponent
  ],
  providers: [{
    
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },
  { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
