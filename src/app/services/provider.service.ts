import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from "../../environments/environment";

import { Provider } from "../models/provider.model";
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  updateProvider(IdUser: number): any {
    throw new Error("Method not implemented.");
  }

  public enviroment = environment;
  
  constructor(private http: HttpClient) { }

  getProviders(){
    return this.http.get<any>(this.enviroment.URL + 'v1/CLIENTES/')
    .pipe(map(providers => {
      let array = providers.Provideres;
      return array;
    }));
  }

  getProviderbyId(idProvider: number) {
    return this.http.get<any>(this.enviroment.URL + `v1/CLIENTES/${idProvider}`);
  }

  createProvider(provider: Provider) {
    return this.http.post(this.enviroment.URL + 'v1/CLIENTES', provider);
  }

  deleteProvider(idProvider: number) {
    return this.http.delete(this.enviroment.URL + `v1/CLIENTES/${idProvider}`);
  }

}
