import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from "../../environments/environment";

import { map } from 'rxjs/operators';
import { Service } from '../models/service.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PackageService {

  public enviroment = environment;

  constructor(private http:HttpClient) {  }
  getAdmins(){
    return this.http.get<any>(this.enviroment.URL + 'v1/PAQUETES/')
    .pipe(map(packages => {
      let array = packages.Paquetes;
      
      return array;
    }));
  }
  
  getAdminbyId(idService: number) {
    return this.http.get<any>(this.enviroment.URL + `v1/PAQUETES/${idService}`);
  }
  
  createAdmin(service: Service) {
    return this.http.post(this.enviroment.URL + 'v1/PAQUETES', service, httpOptions);
  }
  
  updateAdmin(IdUser: number, service: Service) {
    return this.http.put(this.enviroment.URL + `v1/PAQUETES/${IdUser}`, service, httpOptions);
  }

  deleteAdmin(idService: number) {
    return this.http.delete(this.enviroment.URL + `v1/PAQUETES/${idService}`);
  }
}
