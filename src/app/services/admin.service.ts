import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from "../../environments/environment";

import { Admin } from "../models/admin.model";
import { map } from 'rxjs/operators';
import { User } from '../models/user.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  
  public enviroment = environment;
  admin: Admin;
  
  constructor(private http:HttpClient) { }
  
  getAdmins(){
    return this.http.get<any>(this.enviroment.URL + 'v1/ADMINISTRADORES/')
    .pipe(map(admins => {
      let array = admins.Administradores;
      
      return array;
    }));
  }
  
  getAdminbyId(idAdmin: number) {
    return this.http.get<any>(this.enviroment.URL + `v1/ADMINISTRADORES/${idAdmin}`);
  }
  
  createAdmin(admin: Admin) {
    return this.http.post(this.enviroment.URL + 'v1/ADMINISTRADORES', admin);
  }
  
  updateAdmin(IdUser: number, newUser: User) {
    return this.http.put(this.enviroment.URL + `v1/ADMINISTRADORES/${IdUser}`, newUser);
  }

  deleteAdmin(idAdmin: number) {
    return this.http.delete(this.enviroment.URL + `v1/ADMINISTRADORES/${idAdmin}`);
  }

}