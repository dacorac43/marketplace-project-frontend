import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from "../../environments/environment";

import { map } from 'rxjs/operators';
import { Package } from '../models/package.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PackageService {

  public enviroment = environment;

  constructor(private http:HttpClient) {  }

  getPackages(){
    return this.http.get<any>(this.enviroment.URL + 'v1/PAQUETES/')
    .pipe(map(packages => {
      let array = packages.Paquetes;
      
      return array;
    }));
  }
  
  getPackageById(idPackage: number) {
    return this.http.get<any>(this.enviroment.URL + `v1/PAQUETES/${idPackage}`);
  }
  
  createPackage(pack: Package) {
    return this.http.post(this.enviroment.URL + 'v1/PAQUETES', pack, httpOptions);
  }
  
  updatePackage(IdUser: number, pack: Package) {
    return this.http.put(this.enviroment.URL + `v1/PAQUETES/${IdUser}`, pack, httpOptions);
  }

  deletePackage(idPackage: number) {
    return this.http.delete(this.enviroment.URL + `v1/PAQUETES/${idPackage}`);
  }
}
