import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from "../../environments/environment";

import { map } from 'rxjs/operators';
import { Category } from '../models/category.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  public enviroment = environment;

  constructor(private http:HttpClient) {  }

  getCategories(){
    return this.http.get<any>(this.enviroment.URL + 'v1/CATEGORIAS/')
    .pipe(map(categories => {
      let array = categories.Categorias;
      
      return array;
    }));
  }
  
  getCategorybyId(idCategory: number) {
    return this.http.get<any>(this.enviroment.URL + `v1/CATEGORIAS/${idCategory}`);
  }
  
  createCategory(category: Category) {
    return this.http.post(this.enviroment.URL + 'v1/CATEGORIAS', category, httpOptions);
  }
  
  updateCategory(IdUser: number, category: Category) {
    return this.http.put(this.enviroment.URL + `v1/CATEGORIAS/${IdUser}`, category, httpOptions);
  }

  deleteCategory(idCategory: number) {
    return this.http.delete(this.enviroment.URL + `v1/CATEGORIAS/${idCategory}`);
  }
}
