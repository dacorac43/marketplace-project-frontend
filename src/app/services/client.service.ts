import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from "../../environments/environment";

import { Client } from "../models/client.model";
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ClientService {
  updateClient(IdUser: number): any {
    throw new Error("Method not implemented.");
  }

  public enviroment = environment;
  
  constructor(private http:HttpClient) { }

  getClients(){
    return this.http.get<any>(this.enviroment.URL + 'v1/CLIENTES/')
    .pipe(map(clients => {
      let array = clients.Clientes;
      return array;
    }));
  }

  getClientbyId(idClient: number) {
    return this.http.get<any>(this.enviroment.URL + `v1/CLIENTES/${idClient}`);
  }

  createClient(client: Client) {
    return this.http.post(this.enviroment.URL + 'v1/CLIENTES', client);
  }

  deleteClient(idClient: number) {
    return this.http.delete(this.enviroment.URL + `v1/CLIENTES/${idClient}`);
  }

}
