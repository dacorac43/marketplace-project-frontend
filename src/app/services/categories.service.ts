import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

const URL = 'http://localhost:8080'
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http:HttpClient) { }

  getCategories() {
    return this.http.get(URL + '/v1/CATEGORIAS/');
  }

  createCategory(TITULOCATEGORIA:string) {
    let category = {
      TITULOCATEGORIA: TITULOCATEGORIA
    };

    return this.http.post('/v1/CATEGORIAS', category, httpOptions);
  }

  getCategoryById(id:Number) {
    return this.http.get(`/v1/CATEGORIAS/${id}`);
  }
}
