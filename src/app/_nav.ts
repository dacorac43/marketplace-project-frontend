export var navItems = [
  {
    name: 'Inicio',
    url: '/',
    icon: 'icon-home'
  },
  {
    title: true,
    name: 'Productos'
  },
  {
    name: 'Categorías',
    url: '/categories',
    icon: 'icon-list',
    // children: [
    //   {
    //     name: 'Alojamiento',
    //     url: '/category/lodging',
    //     icon: 'icon-list'
    //   },
    //   {
    //     name: 'Alimentación',
    //     url: '/category/feeding',
    //     icon: 'icon-list'
    //   },
    //   {
    //     name: 'Paseos ecológicos',
    //     url: '/category/ecowalks',
    //     icon: 'icon-list'
    //   }
    // ]
  },
  {
    name: 'Carrito de Compras',
    url: '/cart',
    icon: 'icon-basket-loaded'
  },  
  
  {
    name: 'Contenidos',
    url:'home',
    icon: 'icon-note'
  },

];
