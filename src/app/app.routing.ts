import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SaleComponent } from "./pages/sale/sale.component";
import { ProviderComponent } from './pages/provider/provider-detail/provider.component';
import { CartComponent } from './pages/cart/cart.component';
import { ListAdminsComponent } from './pages/admin/list-admins/list-admins.component';
import { ListClientsComponent } from './pages/client/list-clients/list-clients.component';
import { AuthGuard } from './_guards/auth.guard';
import { AdminComponent } from './pages/admin/admin-detail/admin.component';
import { ClientComponent } from './pages/client/client-detail/client.component';
import { ListProvidersComponent } from './pages/provider/list-providers/list-providers.component';
import { ProfileComponent } from './pages/profile/profile.component';

export const routes: Routes = [
  // {
  //   path: '',
  //   // redirectTo: 'home',
  //   pathMatch: 'full',
  // },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Marketplace',
      roles: [1, 2, 3]
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './pages/home/home.module#HomeModule'
      },
      {
        path: 'sale',
        component: SaleComponent
      },
      {
        path: 'provider/:id',
        component: ProviderComponent 
      },
      {
        path: 'cart',
        component: CartComponent
      },
      {
        path: 'list-admins',
        component: ListAdminsComponent,
        canActivate: [AuthGuard],
        data: { roles: [1] }
      },
      {
        path: 'list-admins/detail/:id',
        component: AdminComponent,
        canActivate: [AuthGuard],
        data: { roles: [1] }
      },
      {
        path: 'list-providers',
        component: ListProvidersComponent,
        canActivate: [AuthGuard],
        data: { roles: [1] }
      },
      {
        path: 'list-providers/detail/:id',
        component: ProviderComponent,
        canActivate: [AuthGuard],
        data: { roles: [1] }
      },
      {
        path: 'list-clients',
        component: ListClientsComponent,
        canActivate: [AuthGuard],
        data: { roles: [1] }
      },
      {
        path: 'list-clients/detail/:id',
        component: ClientComponent,
        canActivate: [AuthGuard],
        data: { roles: [1] }
      },
      {
        path: 'edit-profile',
        component: ProfileComponent,
        canActivate: [AuthGuard],
        data: { roles: [1, 2, 3] }
      },
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'icons',
        loadChildren: './views/icons/icons.module#IconsModule'
      },
      {
        path: 'notifications',
        loadChildren: './views/notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'theme',
        loadChildren: './views/theme/theme.module#ThemeModule'
      },
      {
        path: 'widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
