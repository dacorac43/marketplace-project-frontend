import { Component, OnInit, Input } from '@angular/core';
import { navItems } from './../../_nav';

import { CategoriesService } from "../../services/categories.service";
import { LoginService } from "../../services/login.service";
import 'rxjs/Rx';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  constructor(private _categoryService: CategoriesService, private _loginService: LoginService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }

  ngOnInit() {
    // this._categoryService.getCategories().subscribe(cat =>{
    //   console.log(cat);
      
    //   cat.pipe(map(element => {
    //     element.name = element.TITULOCATEGORIA;
    //     element.url = `/category/${element.Id}`;
    //     element.icon = 'icon-list';
    //   }));

    //   console.log(cat);

    //   console.log(this.navItems);
    //   this.navItems.map(element => {
    //     if (element.name == 'Categorías') {
    //       element.children = cat;
    //     }
    //   });
    // });
  }

  logout() {
    this._loginService.logout();
  }
}
