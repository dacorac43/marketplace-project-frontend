import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Auth } from '../models/auth.model';

import { LoginService } from "../services/login.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  data: Auth;
  returnUrl: string;
  error: '';

  constructor(private _loginService: LoginService, 
              private router: Router,
              private route: ActivatedRoute) { 
    // redirect to home if already logged in
  }
  
  ngOnInit() {
    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    if (this._loginService.currentUserValue) { 
      this.router.navigate(['/']);
    }
  }

  authenticate(form) {

    let email = form.controls.email.value;
    let password = form.controls.password.value;
    
    this.data = {
      Correo : email,
      Password : password
    }
    
    this._loginService.login(this.data)
    .subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/']);
      },
      error => {
          this.error = error;
      });
    
  }

}
