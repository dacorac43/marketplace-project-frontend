import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';


@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
  // templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {

  //   this._categoryService.getCategories().subscribe(cat =>{
  //     console.log(cat);
      
  //     cat.map(element => {
  //       element.name = element.TITULOCATEGORIA;
  //       element.url = `/category/${element.Id}`;
  //       element.icon = 'icon-list';
  //     });

  //     console.log(cat);
  //   });
    


    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
