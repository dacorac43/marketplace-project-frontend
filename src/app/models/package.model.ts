import { Provider } from "./provider.model";
import { Service } from "./service.model";

export class Package {
    CiudadDestino: string
    CuposDisponibles: number
    CuposPublicados: number
    Id: number
    Precio: number
    Proveedor: Provider
    Servicios: Service
}