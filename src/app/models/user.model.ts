export class User {
    Id: number;
    Nombre: string;
    Apellido: string;
    Correo: string;
    Rol: number;
    Password?: string;
    Token?: string;
}