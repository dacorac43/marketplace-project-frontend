import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ClientService } from '../../../services/client.service';
import { Client } from '../../../models/client.model';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  idClient: number;
  client: Client;
  
  constructor(private _clientService: ClientService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idClient = params.id;
    });

    this._clientService.getClientbyId(this.idClient)
      .subscribe(client => {
        console.log(client);
        this.client = client;
      });
  }

}
