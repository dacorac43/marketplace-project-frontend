import { Component, OnInit } from '@angular/core';

import { Client } from '../../../models/client.model';

import { ClientService } from '../../../services/client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.scss']
})
export class ListClientsComponent implements OnInit {

  clients: Client[];

  constructor(private _clientService: ClientService, private router: Router) { }

  ngOnInit() {
    this._clientService.getClients()
      .subscribe(clients => this.clients = clients);
  }

  getClientById(id: number) {
    this.router.navigate(['/list-clients/detail/', id]);
  }
}
