import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from '../../../services/admin.service';
import { Admin } from '../../../models/admin.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  idAdmin: number;
  admin: Admin;

  constructor(private _adminService: AdminService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idAdmin = params.id;
    });

    this._adminService.getAdminbyId(this.idAdmin)
      .subscribe(admin => {
        console.log(admin);
        this.admin = admin;
      });
  }

}
