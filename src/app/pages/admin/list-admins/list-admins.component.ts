import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/admin.service';
import { Router } from '@angular/router';

import { Admin } from '../../../models/admin.model';
import { ClientService } from '../../../services/client.service';

@Component({
  selector: 'app-list-admins',
  templateUrl: './list-admins.component.html',
  styleUrls: ['./list-admins.component.scss']
})
export class ListAdminsComponent implements OnInit {

  admins: Admin[];
  constructor(private _adminService: AdminService, private _clientService: ClientService, private router: Router) { }

  ngOnInit() {
    this._adminService.getAdmins()
      .subscribe(admins => {
        this.admins = admins;
        console.log(admins);
      });

    this._clientService.getClients()
      .subscribe(res => console.log(res));
  }

  getAdminById(id: number) {
    this.router.navigate(['/list-admins/detail/', id]);
  }

}
