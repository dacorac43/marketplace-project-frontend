import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { User } from '../../models/user.model';
import { AdminService } from '../../services/admin.service';
import { ProviderService } from '../../services/provider.service';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  currentUser: User;
  newUser: User;
  IdUser: number;
  error: boolean = false;
  constructor(private _loginService: LoginService,
              private _adminService: AdminService,
              private _providerService: ProviderService,
              private _clientService: ClientService) { }

  ngOnInit() {
    this.currentUser = this._loginService.currentUserValue;
    
    this.IdUser = this.currentUser.Id;
    this.newUser = new User();
  }

  onSubmit() {
    this.newUser.Token = this.currentUser.Token;
    this.newUser.Correo = "";
    console.log(this.newUser);

    if(this.currentUser.Rol === 1) {
      this._adminService.updateAdmin(this.IdUser, this.newUser)
        .subscribe(
          res => {
            return this.error = false;
          }, 
          error => {
            if(error) {
              this.error = true; 
            }
        });
    } else if(this.currentUser.Rol === 2) {
      this._providerService.updateProvider(this.IdUser)
      .subscribe(
        res => {
          return this.error = false;
        }, 
        error => {
          if(error) {
            this.error = true; 
          }
      });
    } else if(this.currentUser.Rol === 3) {
      this._clientService.updateClient(this.IdUser)
        .subscribe(
          res => {
            return this.error = false;
          }, 
          error => {
            if(error) {
              this.error = true; 
            }
        });
    }
  }

}
