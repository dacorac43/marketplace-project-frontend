// Angular
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Components
import { HomeComponent } from './home.component';
import { SalesResultComponent } from '../search/salesResult/salesResult.component';
import { ProvidersResultComponent } from '.././search/providers-result/providers-result.component';

// Components Routing
import { HomeRoutingModule } from './home-routing.module';

// Ngx-bootstrap Modules
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationModule } from 'ngx-bootstrap/pagination';

// Services
// import { CategoriesService } from "../../services/categories.service";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CollapseModule,
    PaginationModule,
    HomeRoutingModule,
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [
    HomeComponent,
    SalesResultComponent,
    ProvidersResultComponent
  ]
  // ],
  // providers: [
  //   CategoriesService
  // ]
})

export class HomeModule { }
