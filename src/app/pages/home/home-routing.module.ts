import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { HomeComponent } from './home.component';
import { SalesResultComponent } from '../search/salesResult/salesResult.component';
import { ProvidersResultComponent } from '.././search/providers-result/providers-result.component';
import { AuthGuard } from '../../_guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home',
      roles: [1, 2, 3]
    },
    children: [
      {
        path: 'search',
        component: ProvidersResultComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
