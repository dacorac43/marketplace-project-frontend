import { Component, OnInit } from '@angular/core';
import { CategoriesService } from "../../services/categories.service";
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(private _categoryService: CategoriesService) { }

  ngOnInit() {
    // console.log(this);
    
    // this._categoryService.getCategories()
    //   .subscribe(categories => console.log(categories));
  }

}
