import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProviderService } from '../../../services/provider.service';
import { Provider } from '../../../models/provider.model';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {

  idProvider: number;
  provider: Provider;

  constructor(private _providerService: ProviderService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idProvider = params.id;
    });

    this._providerService.getProviderbyId(this.idProvider)
      .subscribe(provider => {
        console.log(provider);
        this.provider = provider;
      });
  }

}
