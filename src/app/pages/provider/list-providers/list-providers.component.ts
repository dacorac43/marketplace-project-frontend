import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProviderService } from '../../../services/provider.service';
import { Provider } from '../../../models/provider.model';

@Component({
  selector: 'app-list-providers',
  templateUrl: './list-providers.component.html',
  styleUrls: ['./list-providers.component.scss']
})
export class ListProvidersComponent implements OnInit {
  providers: Provider[];

  constructor(private _providerService: ProviderService, private router: Router) { }

  ngOnInit() {
    this._providerService.getProviders()
      .subscribe(providers =>{
        console.log(providers);
        this.providers = providers;
      });
  }

  getProviderById(id: number) {
    this.router.navigate(['/list-providers/detail/', id]);
  }
}
